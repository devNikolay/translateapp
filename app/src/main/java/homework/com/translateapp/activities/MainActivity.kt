package homework.com.translateapp.activities

import android.os.Bundle

import homework.com.translateapp.R
import homework.com.translateapp.fragments.TranslateFragment

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fragment)
        if (savedInstanceState == null) {
            changeFragment(TranslateFragment(), false)
        }
    }
}
