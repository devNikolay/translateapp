package homework.com.translateapp.activities

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import homework.com.translateapp.R


abstract class BaseActivity : AppCompatActivity() {
    private val mFm = supportFragmentManager


    override fun onBackPressed() {
        mFm.popBackStack()
        if (mFm.backStackEntryCount == 0) {
            super.onBackPressed()
        }
    }

    fun goToTop() {
        mFm.popBackStack(mFm.getBackStackEntryAt(0).id, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }

    fun changeFragment(f: Fragment, addToBackStack: Boolean) {
        val ft = mFm.beginTransaction()
        // Backstack
        if (addToBackStack) {
            ft.addToBackStack(null)
        }
        // Adding fragment
        val oldFragment = mFm.findFragmentById(R.id.fragmentContainer)
        if (oldFragment != null) {
            ft.remove(oldFragment)
        }
        ft.add(R.id.fragmentContainer, f)
        // Commit transaction
        ft.commit()
    }
}
