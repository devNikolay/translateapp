package homework.com.translateapp.pojos

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class DetectLang(@Expose @SerializedName("code") val mCode: Int,
                      @Expose @SerializedName("lang") val mLang: String)
