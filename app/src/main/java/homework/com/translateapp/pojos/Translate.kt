package homework.com.translateapp.pojos

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

data class Translate(@Expose @SerializedName("code") val mCode: Int,
                     @Expose @SerializedName("lang") val mLang: String,
                     @Expose @SerializedName("text") val mText: ArrayList<String> = ArrayList<String>())
