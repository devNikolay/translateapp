package homework.com.translateapp.fragments

import android.app.AlertDialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import homework.com.translateapp.R
import homework.com.translateapp.network.NetworkManager
import homework.com.translateapp.network.RequestInterface
import homework.com.translateapp.pojos.DetectLang
import homework.com.translateapp.pojos.Translate
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_translate.*


class TranslateFragment : Fragment() {
    companion object {

        private val ARG_TRANSLATE = "translate"
        private val ARG_DETECT = "detect"
        private val ARG_TRANSLATED = "translated"
        private val API_KEY = "trnsl.1.1.20151211T185148Z.d680a92f690e62f8.5c5c60e5ef974ffaaf04ffffad76c4e16ba4c181"
        private val TAG = "Translate Fragment"
    }

    private var mCompositeDisposable: CompositeDisposable? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (savedInstanceState != null) {
            translateEditText.setText(savedInstanceState.getString(ARG_TRANSLATE))
            detectLangTextView.text = savedInstanceState.getString(ARG_DETECT)
            translatedTextView.visibility = View.VISIBLE
            translatedTextView.text = savedInstanceState.getString(ARG_TRANSLATED)

            Log.d(TAG, translatedTextView.text.toString())
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater!!.inflate(R.layout.fragment_translate, container, false)
        mCompositeDisposable = CompositeDisposable()
        return v
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        val service = NetworkManager.requestInterface
        translatedTextView.visibility = View.INVISIBLE
        initSpinner()
        translateImageButton.setOnClickListener {
            translate(service)
        }
    }

    private fun translate(service: RequestInterface) {
        if (!translateEditText.text.toString().trim().isEmpty()) {
            val textForTranslate = translateEditText.text.toString()
            val selected = selectLangSpinner.selectedItem.toString()
            val lang = selected.substring(selected.lastIndexOf(" ") + 1)

            mCompositeDisposable!!.add(service
                    .getTranslate(API_KEY, textForTranslate, lang)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ bindTranslate(it) }, { handleError(it) }))

            mCompositeDisposable!!.add(service
                    .getDetectLang(API_KEY, textForTranslate)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({bindDetectLang(it)}, {handleError(it)}))
        } else {
            createErrorDialog()
        }
    }

    private fun handleError(error: Throwable) {
        Toast.makeText(activity, "Error " + error.localizedMessage, Toast.LENGTH_SHORT).show()
    }

    private fun createErrorDialog() {
        AlertDialog.Builder(activity)
                .setMessage(R.string.error_message)
                .create()
                .show()
    }

    private fun initSpinner() {
        val spinnerAdapter = ArrayAdapter.createFromResource(context, R.array.lang, android.R.layout.simple_spinner_item)
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        selectLangSpinner.adapter = spinnerAdapter
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState!!.putString(ARG_TRANSLATE, translateEditText.text.toString())
        outState.putString(ARG_DETECT, detectLangTextView.text.toString())
        outState.putString(ARG_TRANSLATED, translatedTextView.text.toString())
        super.onSaveInstanceState(outState)
    }

    override fun onDestroyView() {
        mCompositeDisposable!!.clear()
        super.onDestroyView()
    }

    private fun bindTranslate(translate: Translate) {
        translatedTextView.visibility = View.VISIBLE
        translatedTextView.text = translate.mText[0]
    }

    private fun bindDetectLang(lang: DetectLang) {
        detectLangTextView.text = lang.mLang
    }

}
