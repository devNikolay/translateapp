package homework.com.translateapp.network

import homework.com.translateapp.pojos.DetectLang
import homework.com.translateapp.pojos.Translate
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query


interface RequestInterface {

    @GET("translate")
    fun getTranslate(@Query("key") key: String,
                     @Query("text") text: String,
                     @Query("lang") lang: String): Observable<Translate>

    @GET("detect")
    fun getDetectLang(@Query("key") key: String,
                      @Query("text") text: String): Observable<DetectLang>

}

